#ifndef IMAGE_H
#define IMAGE_H
#include "bmp.h"
#include <stdint.h>
#include <stdlib.h>

struct __attribute__((packed)) pixel {
    uint8_t r, g, b;
};

struct image {
    uint32_t width, height;
    struct pixel *data;
};

struct image create_image(uint32_t width, uint32_t height);

void destroy_image(struct image *img);

#endif
