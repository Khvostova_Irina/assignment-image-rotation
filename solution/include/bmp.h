#ifndef BMP_H
#define BMP_H
#include "image.h"
#include <stdint.h>
#include <stdio.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
struct image;

enum write_status {
    WRITE_OK,
    WRITE_ERROR
};

enum read_status {
    READ_OK,
    READ_INVALID_SIGNATURE,
    READ_INVALID_HEADER,
    READ_INVALID_BITS,
    READ_ERROR
};

void print_image_info(const struct bmp_header *header);

size_t damn_padding(uint32_t width);

enum read_status read_bmp(FILE *in, struct image *img);

enum write_status write_bmp(FILE *out, const struct image *img);

#endif
