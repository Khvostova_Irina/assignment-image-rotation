#ifndef TRANSFORM_H
#define TRANSFORM_H
#include "image.h"

struct image rotate90(const struct image *source, struct image *result);

void display_image(const struct image *img);

#endif
