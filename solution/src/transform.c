#include "../include/transform.h"

void display_image(const struct image *img) {
//    for (uint32_t i = 0; i < img->height; ++i) {
//        for (uint32_t j = 0; j < img->width; ++j) {
//            printf("(%u, %u, %u) ", img->data[i * img->width + j].r,
//                   img->data[i * img->width + j].g,
//                   img->data[i * img->width + j].b);
//        }
//        printf("\n");
//    }
}

struct image rotate90(const struct image *source, struct image *result) {
    printf("Выполняется rotate90...\n");

    result->width = source->height;
    result->height = source->width;

    for (uint32_t i = 0; i < result->height; ++i) {
        for (uint32_t j = 0; j < result->width; ++j) {
            result->data[i * result->width + j].r = source->data[(source->height - j - 1) * source->width + i].r;
            result->data[i * result->width + j].g = source->data[(source->height - j - 1) * source->width + i].g;
            result->data[i * result->width + j].b = source->data[(source->height - j - 1) * source->width + i].b;
        }
    }

    printf("rotate90 завершен.\n");

    return *result;
}

