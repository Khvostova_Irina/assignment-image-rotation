#include "../include/image.h"
#include <stdlib.h>

struct image create_image(uint32_t width, uint32_t height) {
    struct image img;
    img.width = width;
    img.height = height;
    img.data = (struct pixel *) malloc(width * height * sizeof(struct pixel));
    return img;
}

void destroy_image(struct image *img) {
    if (img->data != NULL) {
        free(img->data);
        img->data = NULL;
    }
    img->width = 0;
    img->height = 0;
}
