#include "../include/bmp.h"
#include <stdlib.h>

#define BMP_SIGNATURE 0x4D42
#define BMP_FORMAT_3 40
#define BMP_FORMAT_4 108
#define ERROR_RETURN_CODE 1
#define BIT_COUNT 24
#define SUCCESS_RETURN_CODE 0

void print_image_info(const struct bmp_header *header) {
    printf("bfType: %x\n", header->bfType);
    printf("Зарезервировано: %u\n", header->bfReserved);
    printf("Количество цветовых плоскостей: %u\n", header->biPlanes);
    printf("Способ хранения изображения: %u\n", header->biCompression);
    printf("Размер изображения: %u\n", header->biSizeImage);
    printf("Количество пикселей X: %u\n", header->biXPelsPerMeter);
    printf("Количество пикселей Y: %u\n", header->biYPelsPerMeter);
    printf("Количество используемых цветов в палитре: %u\n", header->biClrUsed);
    printf("Количество \"важных\" цветов: %u\n", header->biClrImportant);
    printf("Количество битов цвета: %u\n", header->biBitCount);
    printf("Размер заголовка: %lu\n", (unsigned long)sizeof(struct bmp_header));
}

size_t damn_padding(uint32_t width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

enum read_status read_bmp(FILE *in, struct image *img) {
    struct bmp_header header;

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        fprintf(stderr, "Ошибка при чтении заголовка изображения\n");
        return READ_ERROR;
    }

    if (header.bfType != BMP_SIGNATURE) {
        fprintf(stderr, "Неверная сигнатура изображения\n");
        return READ_INVALID_SIGNATURE;
    }

    if (header.biSize > BMP_FORMAT_3) {
        if (fread(&(header.biXPelsPerMeter), sizeof(int32_t), 4, in) != 4) {
            fprintf(stderr, "Ошибка при чтении дополнительных данных заголовка изображения\n");
            return READ_ERROR;
        }
    }

    if (header.biSize == BMP_FORMAT_3 || header.biSize == BMP_FORMAT_4) {
        printf("BMP формат: %u\n", header.biSize);
        print_image_info(&header);
    } else {
        fprintf(stderr, "Неверный формат заголовка изображения\n");
        return READ_INVALID_HEADER;
    }

    if (header.biBitCount != BIT_COUNT) {
        fprintf(stderr, "Неверное количество бит на пиксель\n");
        return READ_INVALID_BITS;
    }

    if (header.bOffBits < sizeof(struct bmp_header)) {
        fprintf(stderr, "Неверное значение bOffBits\n");
        return READ_INVALID_HEADER;
    }

    img->width = header.biWidth;
    img->height = abs((int32_t)header.biHeight);
    img->data = (struct pixel *)malloc(img->width * img->height * sizeof(struct pixel));

    if (img->data == NULL) {
        fprintf(stderr, "Ошибка при выделении памяти для данных пикселей изображения\n");
        return READ_ERROR;
    }

    size_t padding = damn_padding(img->width);

    if (fseek(in, header.bOffBits, SEEK_SET) != 0) {
        fprintf(stderr, "Ошибка при сдвиге позиции в файле\n");
        free(img->data);
        return READ_ERROR;
    }

    for (uint32_t i = 0; i < img->height; ++i) {
        if (fread(&(img->data[i * img->width]), sizeof(struct pixel), img->width, in) != img->width) {
            fprintf(stderr, "Ошибка при чтении данных пикселей изображения\n");
            free(img->data);
            return READ_ERROR;
        }

        fseek(in, (long)padding, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status write_bmp(FILE *out, const struct image *img) {
    size_t padding = damn_padding(img->width);

    struct bmp_header header = {
            .bfType = BMP_SIGNATURE,
            .bfReserved = SUCCESS_RETURN_CODE,
            .biPlanes = SUCCESS_RETURN_CODE,
            .biCompression = SUCCESS_RETURN_CODE,
            .biXPelsPerMeter = SUCCESS_RETURN_CODE,
            .biYPelsPerMeter = SUCCESS_RETURN_CODE,
            .biClrUsed = SUCCESS_RETURN_CODE,
            .biClrImportant = SUCCESS_RETURN_CODE,
            .biBitCount = BIT_COUNT,
            .biWidth = img->width,
            .biHeight = img->height,
            .bOffBits = sizeof(struct bmp_header),
            .biSizeImage = img->width * sizeof(struct pixel) * img->height + (padding * img->height),
            .biSize = BMP_FORMAT_4
    };

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        fprintf(stderr, "Ошибка при записи заголовка изображения\n");
        return WRITE_ERROR;
    }

    for (uint32_t i = 0; i < img->height; ++i) {
        if (fwrite(&(img->data[i * img->width]), sizeof(struct pixel), img->width, out) != img->width) {
            fprintf(stderr, "Ошибка при записи данных пикселей изображения\n");
            return WRITE_ERROR;
        }

        for (size_t p = 0; p < padding; ++p) {
            if (fputc(0, out) == EOF) {
                fprintf(stderr, "Ошибка при записи padding\n");
                return WRITE_ERROR;
            }
        }
    }

    print_image_info(&header);

    return WRITE_OK;
}
