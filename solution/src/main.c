#include "../include/bmp.h"
#include "../include/transform.h"
#include <stdio.h>

#define INPUT_FILE_MODE "rb"
#define OUTPUT_FILE_MODE "wb"
#define ERROR_RETURN_CODE 1
#define SUCCESS_RETURN_CODE 0

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Флексят: %s <source-image> <transformed-image>\n", argv[0]);
        return ERROR_RETURN_CODE;
    }

    const char *input_file = argv[1];
    const char *transformed_file = argv[2];

    FILE *input_file_ptr = fopen(input_file, INPUT_FILE_MODE);
    if (!input_file_ptr) {
        fprintf(stderr, "Ошибка при открытии входного файла: %s\n", input_file);
        return ERROR_RETURN_CODE;
    }

    struct image original;
    enum read_status read_status = read_bmp(input_file_ptr, &original);
    fclose(input_file_ptr);

    if (read_status != READ_OK) {
        fprintf(stderr, "Ошибка при чтении изображения: ");
        switch (read_status) {
            case READ_INVALID_SIGNATURE:
                fprintf(stderr, "неверная сигнатура\n");
                break;
            case READ_INVALID_HEADER:
                fprintf(stderr, "неверный заголовок\n");
                break;
            case READ_INVALID_BITS:
                fprintf(stderr, "неверные данные пикселей\n");
                break;
            case READ_ERROR:
                fprintf(stderr, "ошибка во время чтения\n");
                break;
            default:
                fprintf(stderr, "неизвестная ошибка\n");
        }
        return ERROR_RETURN_CODE;
    }

    printf("Изображение успешно прочитано. Ширина: %u, Высота: %u\n", original.width, original.height);

    struct image transformed = create_image(original.height, original.width);

    rotate90(&original, &transformed);

    display_image(&transformed);

    FILE *transformed_file_ptr = fopen(transformed_file, OUTPUT_FILE_MODE);
    if (!transformed_file_ptr) {
        fprintf(stderr, "Ошибка при открытии выходного файла: %s\n", transformed_file);
        destroy_image(&original);
        destroy_image(&transformed);
        return ERROR_RETURN_CODE;
    }

    enum write_status write_status = write_bmp(transformed_file_ptr, &transformed);
    fclose(transformed_file_ptr);

    if (write_status != WRITE_OK) {
        fprintf(stderr, "Ошибка при записи изображения: ");
        switch (write_status) {
            case WRITE_ERROR:
                fprintf(stderr, "ошибка во время записи\n");
                break;
            default:
                fprintf(stderr, "неизвестная ошибка\n");
        }
        destroy_image(&original);
        destroy_image(&transformed);
        return ERROR_RETURN_CODE;
    }

    printf("Изображение успешно записано в файл: %s\n", transformed_file);

    destroy_image(&original);
    destroy_image(&transformed);

    return SUCCESS_RETURN_CODE;
}
